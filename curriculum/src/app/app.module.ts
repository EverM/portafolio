import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { CurriculumComponent } from './components/curriculum/curriculum.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { RedesComponent } from './components/redes/redes.component';
import { SobreMiComponent } from './components/sobre-mi/sobre-mi.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ServiciosComponent,
    ContactoComponent,
    CurriculumComponent,
    AgendaComponent,
    RedesComponent,
    SobreMiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
